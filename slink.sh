#!/bin/bash
inc=0

echo "Downloading start of stream."

streamlink --hls-live-restart --hls-live-edge 99999 --hls-segment-threads 5 -o "$2-0.ts" $1 best

echo "Remux $2-0.ts"
ffmpeg -i "$2-0.ts" -codec copy "$2-0.mp4" 2>&1 &

echo "First stream interrupted. Going live."

for inc in `seq 1 10`;
do
  streamlink --hls-segment-threads 5 -o "$2-$inc.ts" $1 best
  echo "Stream interrupted, retrying in 10 seconds"
  echo "Remux $2-$inc.ts"
  ffmpeg -i "$2-$inc.ts" -codec copy "$2-$inc.mp4" 2>&1 &
  sleep 10
done

echo "Stream downloading ended!"
